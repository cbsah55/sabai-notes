package com.cbs.sabainotes.utils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class PdfToImageExtractorTest {


    @Test
    public void extractThumbnail(){
        MultipartFile file = getMultipartFile();

        // File file = new File("/home/chaitanya/Documents/Words That Change Minds The 14 Patterns for Mastering the Language of Influence by Shelle Rose Charvet (z-lib.org).pdf");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        try {
            PDDocument pdfDocument = PDDocument.load(convertMultiPartToFile(file));
            PDFRenderer pdfRenderer = new PDFRenderer(pdfDocument);
            BufferedImage coverImage = pdfRenderer.renderImage(0);
            ImageIO.write(coverImage,"jpeg",outputStream);
            inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            pdfDocument.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

            /*InputStream is = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());

            BufferedImage resizedImage = new BufferedImage(300,500,BufferedImage.TYPE_INT_RGB);
            resizedImage.createGraphics().drawImage(ImageIO.read(is).getScaledInstance(300,500,Image.SCALE_SMOOTH),0,0,null);
            ImageIO.write(resizedImage,"JPEG",new File("sample.jpeg"));
*/


    }

    private MultipartFile getMultipartFile() {
        Path path = Paths.get("/home/chaitanya/Documents/Chaitanya's Resume-2021_FEB.pdf");
        byte[] content = null;
        try {
            content = Files.readAllBytes(path);
        } catch (final IOException e) {
        }
        MultipartFile file = new MockMultipartFile("file.pdf",
                "file.pdf", MediaType.APPLICATION_PDF_VALUE, content);
        return file;
    }

    @Test
      void extractCoverImage(MultipartFile file){
        Map<String,byte[]> imageDetails = new HashMap<>();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        try {
            PDDocument pdfDocument = PDDocument.load(convertMultiPartToFile(getMultipartFile()));
            PDFRenderer pdfRenderer = new PDFRenderer(pdfDocument);
            BufferedImage coverImage = pdfRenderer.renderImage(0);
            ImageIO.write(coverImage,"jpeg",outputStream);
            inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            pdfDocument.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageDetails.put("coverImage", createImage(inputStream,300,500));
        imageDetails.put("thumbNail", createImage(inputStream,100,75));
        int i =1;
    }

    private static byte[] createImage(InputStream inputStream, int height, int width){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BufferedImage resizedImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        try {
            resizedImage.createGraphics().drawImage(ImageIO.read(inputStream).getScaledInstance(width,height, Image.SCALE_SMOOTH),0,0,null);
            ImageIO.write(resizedImage,"JPEG",outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }

    private static File convertMultiPartToFile(MultipartFile file ) throws IOException
    {
        File convFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }



}