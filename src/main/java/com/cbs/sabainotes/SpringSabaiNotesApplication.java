package com.cbs.sabainotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSabaiNotesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSabaiNotesApplication.class, args);
    }

}
