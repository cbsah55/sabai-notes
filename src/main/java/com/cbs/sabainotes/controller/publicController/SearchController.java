package com.cbs.sabainotes.controller.publicController;

import com.cbs.sabainotes.service.courses.SearchService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@RestController
@RequestMapping("/public")
@RequiredArgsConstructor
public class SearchController {
    private final SearchService searchService;




    @GetMapping("/subjects")
    public ResponseEntity<?> getSubjects(@RequestParam @NotNull String filter){
        return LocalResponse.configure(searchService.getAllSubjectsByFilter(filter));
    }

    @GetMapping("/materials-filter")
    public ResponseEntity<?> getMaterials(@RequestParam @NotNull String filter){
        return LocalResponse.configure(searchService.getAllMaterialsByFilter(filter));
    }

    @GetMapping("/courses")
    public ResponseEntity<?> getCourses(@RequestParam @NotNull String filter){
        return LocalResponse.configure(searchService.getAllCoursesByFilter(filter));
    }

    @GetMapping("/institutes")
    public ResponseEntity<?> getInstitutes(@RequestParam @NotNull String filter){
        return LocalResponse.configure(searchService.getAllInstitutesByFilter(filter));
    }

    @GetMapping("/faculties")
    public ResponseEntity<?> getFaculties(@RequestParam @NotNull String filter){
        return LocalResponse.configure(searchService.getAllFacultiesByFilter(filter));
    }

    @GetMapping("/academic-sessions")
    public ResponseEntity<?> getALl(@RequestParam(required = false) Optional<Long> id){
        return LocalResponse.configure(searchService.getALlAcademicSessions());
    }

    @GetMapping("/material-types")
    public ResponseEntity<?> getMaterialTypes(){
        return LocalResponse.configure(searchService.getMaterialTypes());
    }



}
