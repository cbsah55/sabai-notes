package com.cbs.sabainotes.controller.publicController;

import com.cbs.sabainotes.service.courses.MaterialService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.apache.pdfbox.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("/public")
@RequiredArgsConstructor
public class PublicMaterialController {
    private final MaterialService materialService;

    @GetMapping("/materials")
    public ResponseEntity<?> getById(@RequestParam long id){
        return LocalResponse.configure(materialService.getModelById(id));
    }

    @PostMapping("/material-rating")
    public ResponseEntity<?> materialRating(@RequestParam long id,
                                            @RequestParam double value){
        materialService.updateMaterialRating(id,value);
        return LocalResponse.configure("Rated successfully");
    }

    @GetMapping("/material-cover")
    public void getCover(HttpServletResponse servletResponse, @RequestParam long id) throws IOException {
        var material = materialService.getById(id);
        byte[] cover = material.getCoverData();
        InputStream inputStream = new ByteArrayInputStream(cover);
        IOUtils.copy(inputStream,servletResponse.getOutputStream());
    }

    @GetMapping("/material-thumb")
    public void getThumb(HttpServletResponse servletResponse, long id) throws IOException {
        var material = materialService.getById(id);
        byte[] cover = material.getThumbNailData();
        InputStream inputStream = new ByteArrayInputStream(cover);
        IOUtils.copy(inputStream,servletResponse.getOutputStream());
    }

    @GetMapping("/material-url")
    public void getData(HttpServletResponse servletResponse, long id) throws IOException {
        var material = materialService.getById(id);
        materialService.updateViewCount(material);
        byte[] cover = material.getFileData();
        InputStream inputStream = new ByteArrayInputStream(cover);
        servletResponse.setContentType(MediaType.APPLICATION_PDF_VALUE);
        servletResponse.setHeader("Content-Disposition","attachment;filename="+material.getName()+".pdf");

        IOUtils.copy(inputStream,servletResponse.getOutputStream());
    }

}
