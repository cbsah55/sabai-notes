package com.cbs.sabainotes.controller.publicController;

import com.cbs.sabainotes.dto.auth.LoginRequestDto;
import com.cbs.sabainotes.dto.auth.SignUpDto;
import com.cbs.sabainotes.service.auth.AuthService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/public")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequestDto loginRequestDto){
        return LocalResponse.configure(authService.authenticate(loginRequestDto));
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpDto signUpDto){
        authService.register(signUpDto);
        return LocalResponse.configure("User Has been registered Successfully");
    }

    @GetMapping("/test")
    public ResponseEntity<?> tset(){
        return ResponseEntity.ok().body("this is test message");
    }

    @PostMapping("/passwordResetToken")
    public ResponseEntity<?> sendPasswordResetVerification(@RequestParam @NotNull String email){
        authService.passwordResetToken(email);
        return LocalResponse.configure("Email has been sent successfully with the verification code.Please use that code to reset your password.");
    }

    @PostMapping("/resetPassword")
    public ResponseEntity<?> resetPassword(@RequestParam @NotNull String token,
                                           @RequestParam(name = "password") @NotBlank String newPassword){
        authService.resetPassword(token,newPassword);
        return LocalResponse.configure("Your password has been changed successfully.Please login using your new password.");
    }
}
