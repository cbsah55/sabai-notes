package com.cbs.sabainotes.controller.userController;

import com.cbs.sabainotes.dto.courses.MaterialDto;
import com.cbs.sabainotes.entity.courses.MaterialTypeEnum;
import com.cbs.sabainotes.service.courses.MaterialService;
import com.cbs.sabainotes.service.courses.UserFavouritesService;
import com.cbs.sabainotes.utils.LocalResponse;
import com.cbs.sabainotes.utils.RequestContext;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/secure/user")
@RequiredArgsConstructor
public class UserMaterialController {

    private final MaterialService materialService;
    private final UserFavouritesService userFavouritesService;

    @PostMapping("/material-upload")
    public ResponseEntity<?> upload(@RequestParam @NotNull MultipartFile file) throws IOException {
        return LocalResponse.configure(materialService.upload(file));
    }

    /*
    TODO need to place update function to material manage controller
     */

    @PutMapping("/material-update")
    public ResponseEntity<?> update(@RequestBody @Valid MaterialDto materialDto){
        return LocalResponse.configure(materialService.update(materialDto));
    }

    @GetMapping("/myMaterials")
    public ResponseEntity<?> getMaterials(@RequestParam(required = false)Optional<Long> id){
        var userId = RequestContext.getUserId();
        if (id.isPresent()){
            return LocalResponse.configure(materialService.getModelByIdAndUserId(id.get(),userId));
        }else {
            return LocalResponse.configure(materialService.getAllContributorMaterials());
        }
    }

    @GetMapping("/materials")
    public ResponseEntity<?> getMaterialsByType(@RequestParam long subjectId,
                                                @RequestParam MaterialTypeEnum type){
        return LocalResponse.configure(materialService.getAllBySubjectAndType(subjectId, type));
    }

    @DeleteMapping("/myMaterial")
    public ResponseEntity<?> deleteMaterial(@RequestParam long materialId){
        var userId = RequestContext.getUserId();
        materialService.deleteByUserIdAndMaterialID(userId,materialId);
        return LocalResponse.configure("Your material have been deleted successfully.");
    }

    @PostMapping("/favourite")
    public ResponseEntity<?> createFavourite(@RequestParam long materialId) {
        userFavouritesService.create(materialId);
        return LocalResponse.configure("Saved to favourites");
    }

    @DeleteMapping("/favourite")
    public ResponseEntity<?> deleteFavourite(@RequestParam long materialId) {
        userFavouritesService.deleteByMaterialId(materialId);
        return LocalResponse.configure("Removed Favourite.");
    }

    @GetMapping("/favourites")
    public ResponseEntity<?> getAllFavourites(){
        return LocalResponse.configure(userFavouritesService.getALlByUser());
    }

}
