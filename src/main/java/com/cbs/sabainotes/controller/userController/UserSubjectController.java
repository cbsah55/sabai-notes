package com.cbs.sabainotes.controller.userController;

import com.cbs.sabainotes.service.courses.SearchService;
import com.cbs.sabainotes.service.user.UserSubjectService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secure/user")
@RequiredArgsConstructor
public class UserSubjectController {
    private final UserSubjectService userSubjectService;
    private final SearchService searchService;

    @PostMapping("/addMySubject")
    public ResponseEntity<?> createUserSubject(@RequestParam long subjectId){
        userSubjectService.addUserSubject(subjectId);
        return LocalResponse.configure("Your subjects have been saved");
    }

    @DeleteMapping("/deleteMySubject")
    public ResponseEntity<?> deleteUserSubject(@RequestParam long subjectId){
        userSubjectService.deleteUserSubject(subjectId);
        return LocalResponse.configure("Your subject have been deleted.");
    }

    @GetMapping("/getMySubjects")
    public ResponseEntity<?> getUserSubjects(){
        return LocalResponse.configure(userSubjectService.getUserSubjects());
    }

    @GetMapping("/field-of-studies")
    public ResponseEntity<?> getFieldOfStudies(){
        return LocalResponse.configure(searchService.getALlFieldOfStudies());
    }

    @GetMapping("/courses")
    public ResponseEntity<?> getALlCourses(@RequestParam(name = "id") long fieldOfStudyId){
        return LocalResponse.configure(searchService.getALlCourses(fieldOfStudyId));
    }

    @GetMapping("/course-faculty")
    public ResponseEntity<?> getALlCourseFaculty(@RequestParam(name = "id") long courseId){
        return LocalResponse.configure(searchService.getALlCourseFaculty(courseId));
    }

    @GetMapping("/course-faculty-subjects")
    public ResponseEntity<?> getAllSubjectsByCourseFaculty(@RequestParam(name = "id") long courseFacultyId){
        return LocalResponse.configure(searchService.getALlSubjectsByCourseFaculty(courseFacultyId));
    }


}
