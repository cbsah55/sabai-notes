package com.cbs.sabainotes.controller.moderator;

import com.cbs.sabainotes.service.auth.AuthService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secure/mod")
@RequiredArgsConstructor
public class ModUserController {

    private final AuthService authService;

    @PutMapping("/user/enable")
    public ResponseEntity<?> enableUser(@RequestParam long id){
        authService.enableUser(id);
        return LocalResponse.configure("User enabled Successfully.");
    }
}
