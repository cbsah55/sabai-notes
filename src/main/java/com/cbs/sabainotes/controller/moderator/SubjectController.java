package com.cbs.sabainotes.controller.moderator;

import com.cbs.sabainotes.dto.courses.SubjectDto;
import com.cbs.sabainotes.dto.courses.SubjectTopicDto;
import com.cbs.sabainotes.service.courses.SubjectTopicService;
import com.cbs.sabainotes.service.courses.impl.SubjectServiceImpl;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/secure/mod")
@RequiredArgsConstructor
public class SubjectController {

    private final SubjectServiceImpl subjectServiceImpl;
    private final SubjectTopicService subjectTopicService;


    @PostMapping("/subject")
    public ResponseEntity<?> create(@RequestBody @Valid SubjectDto subjectDto){
        return LocalResponse.configure(subjectServiceImpl.create(subjectDto));
    }

    @PutMapping("/subject")
    public ResponseEntity<?> update(@RequestBody @Valid SubjectDto subjectDto){
        return LocalResponse.configure(subjectServiceImpl.update(subjectDto));
    }

    @GetMapping("/subjects")
    public ResponseEntity<?> getALl(@RequestParam(required = false) Optional<Long> id){
        if (id.isPresent()){
            return LocalResponse.configure(subjectServiceImpl.getModelById(id.get()));
        }else{
            return LocalResponse.configure(subjectServiceImpl.getModels());
        }
    }

    @DeleteMapping("/subject")
    public ResponseEntity<?> delete(long id){
        subjectServiceImpl.deleteById(id);
        return LocalResponse.configure("Subject deleted successfully.");
    }

    @PostMapping("/subject-topic  ")
    public ResponseEntity<?> createTopic(@RequestBody @Valid SubjectTopicDto topicDto){
        return LocalResponse.configure(subjectTopicService.create(topicDto));
    }

    @PutMapping("/subject-topi")
    public ResponseEntity<?> updateTopic(@RequestBody @Valid SubjectTopicDto topicDto){
        return LocalResponse.configure(subjectTopicService.update(topicDto));
    }

    @GetMapping("/subject-topics")
    public ResponseEntity<?> updateTopic(@RequestParam(required = false) Optional<Long> id){
        if (id.isPresent()){
            return LocalResponse.configure(subjectTopicService.getById(id.get()));
        }else{
            return LocalResponse.configure(subjectTopicService.getALl());
        }
    }

    @DeleteMapping("/subject-topic")
    public ResponseEntity<?> deleteTopic(long id){
        subjectTopicService.deleteById(id);
        return LocalResponse.configure("topic deleted successfully.");
    }

}
