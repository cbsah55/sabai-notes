package com.cbs.sabainotes.controller.moderator;

import com.cbs.sabainotes.dto.courses.CourseFacultyDto;
import com.cbs.sabainotes.service.courses.CourseFacultyService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/secure/mod")
@RequiredArgsConstructor
public class CourseFacultyController {

    private final CourseFacultyService courseFacultyService;

    @PostMapping("/course-faculty")
    public ResponseEntity<?> create(@RequestBody @Valid CourseFacultyDto courseFacultyDto){
        return LocalResponse.configure(courseFacultyService.create(courseFacultyDto));
    }

    @PutMapping("/course-faculty")
    public ResponseEntity<?> update(@RequestBody @Valid CourseFacultyDto courseFacultyDto){
        return LocalResponse.configure(courseFacultyService.update(courseFacultyDto));
    }

    @GetMapping("/course-faculties")
    public ResponseEntity<?> update(@RequestParam(required = false) Optional<Long> id){
        if (id.isPresent()){
            return LocalResponse.configure(courseFacultyService.getModelById(id.get()));
        }else{
            return LocalResponse.configure(courseFacultyService.getModels());
        }
    }

    @DeleteMapping("/course-faculty")
    public ResponseEntity<?> delete(long id){
        courseFacultyService.deleteById(id);
        return LocalResponse.configure("Course Faculty deleted successfully.");
    }
}
