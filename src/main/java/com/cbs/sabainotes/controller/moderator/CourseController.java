package com.cbs.sabainotes.controller.moderator;

import com.cbs.sabainotes.dto.courses.CourseDto;
import com.cbs.sabainotes.service.courses.CourseService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/secure/mod")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @PostMapping("/course")
    public ResponseEntity<?> create(@RequestBody @Valid CourseDto courseDto){
        return LocalResponse.configure(courseService.create(courseDto));
    }

    @PutMapping("/course")
    public ResponseEntity<?> update(@RequestBody @Valid CourseDto courseDto){
        return LocalResponse.configure(courseService.update(courseDto));
    }

    @GetMapping("/courses")
    public ResponseEntity<?> get(@RequestParam(required = false)Optional<Long> id){
        if (id.isPresent()){
            return LocalResponse.configure(courseService.getModelById(id.get()));
        }else{
            return LocalResponse.configure(courseService.getModels());
        }
    }

    @DeleteMapping("/course")
    public ResponseEntity<?> delete(long id){
        courseService.deleteById(id);
        return LocalResponse.configure("Course deleted successfully.");
    }

}
