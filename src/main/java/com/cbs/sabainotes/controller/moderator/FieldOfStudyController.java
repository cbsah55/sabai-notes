package com.cbs.sabainotes.controller.moderator;

import com.cbs.sabainotes.entity.courses.FieldOfStudies;
import com.cbs.sabainotes.service.courses.FieldOfStudyService;
import com.cbs.sabainotes.service.courses.SearchService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/secure/mod")
@RequiredArgsConstructor
public class FieldOfStudyController {

    private final FieldOfStudyService fieldOfStudyService;
    private final SearchService searchService;

    @PostMapping("/field-of-study")
    public ResponseEntity<?> create(@RequestBody @Valid FieldOfStudies fieldOfStudy){
        return LocalResponse.configure(fieldOfStudyService.create(fieldOfStudy));
    }

    @GetMapping("/field-of-studies")
    public ResponseEntity<?> get(@RequestParam(required = false) Optional<Long> id){
        if (id.isPresent()){
            return LocalResponse.configure(fieldOfStudyService.getById(id.get()));
        }else{
            return LocalResponse.configure(fieldOfStudyService.getAll());
        }
    }

    @DeleteMapping("/field-of-study")
    public ResponseEntity<?> delete(long id){
        fieldOfStudyService.deleteById(id);
        return LocalResponse.configure("Field Of Study deleted successfully.");
    }
    @GetMapping("/academic-sessions")
    public ResponseEntity<?> getALl(@RequestParam(required = false) Optional<Long> id){
        return LocalResponse.configure(searchService.getALlAcademicSessions());
    }

    @GetMapping("/material-types")
    public ResponseEntity<?> getMaterialTypes(){
        return LocalResponse.configure(searchService.getMaterialTypes());
    }

}
