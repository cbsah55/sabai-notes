package com.cbs.sabainotes.controller.moderator;

import com.cbs.sabainotes.service.courses.MaterialService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secure/mod")
@RequiredArgsConstructor
public class MaterialController {

    private final MaterialService materialService;

    @PutMapping("/verify-material")
    public ResponseEntity<?> verifyMaterial(@RequestParam long id){
        return LocalResponse.configure(materialService.verifyMaterial(id));
    }

    @DeleteMapping("/material")
    public ResponseEntity<?> deleteMaterial(@RequestParam long materialId){
        materialService.deleteById(materialId);
        return LocalResponse.configure("Your material have been deleted successfully.");
    }

    @GetMapping("/materials")
    public ResponseEntity<?> getAll(@RequestParam Boolean verified){
            return LocalResponse.configure(materialService.getAllVerified(verified));
    }


}
