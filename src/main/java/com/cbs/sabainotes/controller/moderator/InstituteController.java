package com.cbs.sabainotes.controller.moderator;

import com.cbs.sabainotes.entity.courses.Institute;
import com.cbs.sabainotes.service.courses.InstituteService;
import com.cbs.sabainotes.utils.LocalResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/secure/mod")
@RequiredArgsConstructor
public class InstituteController {

    private final InstituteService instituteService;

    @PostMapping("/institute")
    public ResponseEntity<?> create(@RequestBody @Valid Institute institute){
        return LocalResponse.configure(instituteService.create(institute));
    }

    @PutMapping("/institute")
    public ResponseEntity<?> update(@RequestBody @Valid Institute institute){
        return LocalResponse.configure(instituteService.update(institute));
    }

    @GetMapping("/institutes")
    public ResponseEntity<?> update(@RequestParam(required = false) Optional<Long> id){
        if (id.isPresent()){
            return LocalResponse.configure(instituteService.getById(id.get()));
        }else{
            return LocalResponse.configure(instituteService.getALl());
        }
    }

    @DeleteMapping("/institute")
    public ResponseEntity<?> delete(long id){
        instituteService.deleteById(id);
        return LocalResponse.configure("institute deleted successfully.");
    }
}
