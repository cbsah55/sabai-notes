package com.cbs.sabainotes.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@NoArgsConstructor
@Getter
@Setter
public class ErrorResponse {
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC")
    private LocalDateTime timestamp;
    private int messageCode;
    private String message;
    private boolean error;
    private List<?> objects;

    public ErrorResponse(String message, int messageCode) {
        this.timestamp = LocalDateTime.now();
        this.messageCode = messageCode;
        this.message = message;
        this.error = true;
        this.objects = null;
    }

    public ErrorResponse(String message){
        this.timestamp = LocalDateTime.now();
        this.messageCode = 99;
        this.message = message;
        this.error = true;
    }



}
