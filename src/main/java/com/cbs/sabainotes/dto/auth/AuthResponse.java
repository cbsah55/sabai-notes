package com.cbs.sabainotes.dto.auth;

import java.util.List;


public class AuthResponse {
    private Long id;
    private String name;
    private String email;
    private List<String> roles;
    private String jwt;

    public String getName() {
        return name;
    }

    public AuthResponse setName(String name) {
        this.name = name;
        return this;
    }

    public Long getId() {
        return id;
    }

    public AuthResponse setId(Long id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public AuthResponse setEmail(String email) {
        this.email = email;
        return this;
    }

    public List<String> getRoles() {
        return roles;
    }

    public AuthResponse setRoles(List<String> roles) {
        this.roles = roles;
        return this;
    }

    public String getJwt() {
        return jwt;
    }

    public AuthResponse setJwt(String jwt) {
        this.jwt = jwt;
        return this;
    }
}
