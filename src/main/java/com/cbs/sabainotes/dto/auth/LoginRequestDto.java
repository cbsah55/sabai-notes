package com.cbs.sabainotes.dto.auth;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Builder(toBuilder = true)
@Value
public class LoginRequestDto {

    @Email(message = "Email must be a valid email address.")
    private String email;
    @NotBlank(message = "password cannot be blank")
    private String password;
}
