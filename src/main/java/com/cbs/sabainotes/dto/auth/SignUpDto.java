package com.cbs.sabainotes.dto.auth;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class SignUpDto {

    private String email;
    private String password;
    private String phoneNo;
    private String firstName;
    private String lastName;


}
