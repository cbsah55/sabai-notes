package com.cbs.sabainotes.dto.courses;

import com.cbs.sabainotes.entity.courses.AcademicSessionEnum;
import com.cbs.sabainotes.entity.courses.MaterialTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MaterialDto {

    private long id;
    @NotBlank(message = "name cannot be null or blank")
    private String name;
    private String cover; // cover of file uploaded can be url or image itself
    private String thumbNail;
    private String description;
    private boolean handWritten;
    private MaterialTypeEnum type; // note, video
    private boolean downloadable;
    private boolean printable;
    @JsonProperty(value = "isPublic")
    private boolean isPublic;
    private Long subjectId;
    private Long instituteId;
    private AcademicSessionEnum academicSession;
}
