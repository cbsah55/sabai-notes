package com.cbs.sabainotes.dto.courses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseDto {
    private long id;
    private String name;
    private long fieldOfStudyId;
}
