package com.cbs.sabainotes.dto.courses;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class SubjectDto {
    private long id;
    private String name;
    private String shortName;
    private String slug;
    private String tag;
    private long facultyId;

}
