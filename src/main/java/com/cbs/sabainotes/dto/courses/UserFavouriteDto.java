package com.cbs.sabainotes.dto.courses;

import lombok.Data;

import javax.validation.constraints.NotNull;
@Data
public class UserFavouriteDto {
    @NotNull(message = "userId should not be null")
    private Long userId;
    @NotNull(message = "materialId should not be null")
    private Long materialId;
}
