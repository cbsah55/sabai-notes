package com.cbs.sabainotes.dto.courses;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
@Data
public class CourseFacultyDto {

    private Long id;
    @NotBlank(message = "name field cannot be blank or null.")
    private String name;
    private String shortName;
    @NotNull(message = "faculty cannot exist without course")
    private Long courseId;
}
