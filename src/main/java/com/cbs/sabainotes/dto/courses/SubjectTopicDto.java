package com.cbs.sabainotes.dto.courses;

import lombok.Data;

@Data
public class SubjectTopicDto {

    private Long id;
    private String name;
    private Long subjectId;
}
