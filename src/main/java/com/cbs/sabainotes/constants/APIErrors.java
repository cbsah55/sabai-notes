package com.cbs.sabainotes.constants;

import lombok.Getter;

@Getter
public enum APIErrors {

    SUCCESS(1,"success"),
    INVALID_REQUEST_PARAM(2,"invalid parameter provided");

    private int code;
    private  String message;

    APIErrors(int i, String success) {
        this.message = success;
    }


}
