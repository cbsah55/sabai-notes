package com.cbs.sabainotes.config.jwt;

import org.springframework.security.core.Authentication;

public interface JwtService {
    String generateJwtToken(Authentication authentication);

    String getEmailFromJwtToken(String token);

    boolean validateJwtToken(String authToken);
}
