package com.cbs.sabainotes.config.jwt;

import com.cbs.sabainotes.entity.user.Role;
import com.cbs.sabainotes.entity.user.UserRole;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.repository.UserRepository;
import com.cbs.sabainotes.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cbs.sabainotes.utils.JwtUtils.getKey;


@Component
@RequiredArgsConstructor
public class JwtServiceImpl implements JwtService {
    private static final Logger logger = LoggerFactory.getLogger(JwtServiceImpl.class);

    @Value("${jwt.expire}")
    private int jwtExpirationMs;

    private final UserRepository userRepository;


    @Override
    public String generateJwtToken(Authentication authentication){
        Map<String,Object> claims = new HashMap<>();
        UserDetails userPrincipal = (UserDetails) authentication.getPrincipal();
        return createToken(claims,userPrincipal.getUsername());
    }

    private String createToken(Map<String, Object> claims, String subject) {
        Map<String,Object> userDetails = new HashMap<>();
        var user = userRepository.findByEmail(subject)
                .orElseThrow(()-> new NotFoundException(String.format("User not found with email",subject)));
        userDetails.put(JwtUtils.ClaimFields.USER_ID,user.getId());
        var roles = user.getRoles().stream()
                .map(Role::getName)
                .map(UserRole::name)
                .collect(Collectors.toList());
        userDetails.put(JwtUtils.ClaimFields.ROLES,roles);
        logger.info("Jwt generated successfully!");
        return Jwts.builder()
                .setClaims(claims)
                .addClaims(userDetails)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() +jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, getKey())
                .compact();
    }

    @Override
    public String getEmailFromJwtToken(String token){
        return Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token).getBody().getSubject();
    }

    private <T> T extractClaims (String token, Function<Claims,T> claimsResolver){
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token).getBody();
    }

    @Override
    public boolean validateJwtToken(String authToken){
        try {
            Jwts.parser().setSigningKey(getKey()).parseClaimsJws(authToken);
            return true;
        }catch (SignatureException e){
            logger.error("Invalid Jwt signature: {}",e.getMessage());
        }catch (MalformedJwtException e){
            logger.error("Invalid JWT token: {}",e.getMessage());
        }catch(ExpiredJwtException e){
            logger.error("JWT token is expired: {}",e.getMessage());
        }catch (UnsupportedJwtException e){
            logger.error("JWT token is unsupported: {}",e.getMessage());
        }catch (IllegalArgumentException e){
            logger.error("JWT claims string is empty: {}",e.getMessage());
        }
        return false;
    }

}
