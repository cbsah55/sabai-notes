package com.cbs.sabainotes.mappers;

import com.cbs.sabainotes.dto.courses.SubjectDto;
import com.cbs.sabainotes.entity.courses.Subject;
import com.cbs.sabainotes.model.courses.SubjectModel;
import com.cbs.sabainotes.utils.LocalStringUtils;

public class SubjectMapper {

    public static Subject dtoToEntity(SubjectDto subjectDto) {
        Subject subject = new Subject();
        subject.setId(subjectDto.getId());
        subject.setName(subjectDto.getName());
        subject.setShortName(subjectDto.getName());
        subject.setSlug(LocalStringUtils.createSlug(subjectDto.getName()));
        subject.setTag(LocalStringUtils.createTag(subjectDto.getName()));
        return subject;
    }


    public static SubjectModel entityToModel(Subject subject){
        return SubjectModel.builder()
                .id(subject.getId())
                .name(subject.getName())
                .shortName(subject.getShortName())
                .slug(subject.getSlug())
                .faculty(subject.getFaculty())
                .subjectTopics(subject.getSubjectTopics())
                .build();
    }
}
