package com.cbs.sabainotes.utils;

import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Collectors;

public class LocalStringUtils {


    public static String processShortName(String value){
        String[] values = value.split(" ");
        return Arrays.stream(values)
                .map(String::toLowerCase)
                .collect(Collectors.joining("-"));
    }

    public static String createSlug(String value){
        var values = Arrays.asList(value.split(" "));

            return values.stream().map(v-> {
                if (v.length()<3){
                    return v.toLowerCase(Locale.ROOT);
                }
                return v.substring(0,3);
            }).map(String::toLowerCase)
                    .collect(Collectors.joining());
    }

    public static String createTag(String value){
        var values = Arrays.asList(value.split(" "));

        return values.stream().map(String::toLowerCase)
                .collect(Collectors.joining("-"));
    }


    public static String getProcessedFileName(String originalFileName,String textToAppend){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(StringUtils.trimWhitespace(originalFileName))
                .append(" - By ")
                .append(textToAppend);
        return stringBuilder.toString();
    }

}
