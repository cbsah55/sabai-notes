package com.cbs.sabainotes.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.function.Function;

import static com.cbs.sabainotes.utils.JwtUtils.getKey;

public class RequestContext {


    public static long getUserId() {
        Jws<Claims> jws = extractJws();
        return Long.valueOf((Integer) jws.getBody().get(JwtUtils.ClaimFields.USER_ID));
    }

    public static List<String> getUserRoles() {
        Jws<Claims> jws = extractJws();
        return (List<String>) jws.getBody().get(JwtUtils.ClaimFields.ROLES);
    }

    public static String getUserName() {
        Jws<Claims> jsw = extractJws();
        return extractClaims(Claims::getSubject);
    }

    private static Jws<Claims> extractJws() {
        String token = extractToken();
        String username = extractClaims(Claims::getSubject);
        return Jwts.parser().requireSubject(username)
                .setSigningKey(JwtUtils.ClaimFields.SECRET_KEY)
                .parseClaimsJws(token);
    }

    private static <T> T extractClaims(Function<Claims, T> claimsResolver) {
        String token = extractToken();
        final Claims claims = Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token).getBody();
        return claimsResolver.apply(claims);
    }

    public static String extractToken() {
        HttpServletRequest request = getSecureServletRequest();
        String authorizationHeader = request.getHeader("Authorization");
        return authorizationHeader.substring(7);
    }

    public static HttpServletRequest getSecureServletRequest() {
        HttpServletRequest servletRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String authHeader = servletRequest.getHeader("Authorization");
        Assert.isTrue(StringUtils.hasText(authHeader), "You are trying to access secure servlet request without authenticating.");
        Assert.isTrue(authHeader.contains("Bearer"), "You are trying to access secure servlet request without authenticating.");
        return servletRequest;
    }


    public static boolean isSecureRequest() {
        HttpServletRequest servletRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String authHeader = servletRequest.getHeader("Authorization");
        return authHeader != null && authHeader.contains("Bearer");
    }
}
