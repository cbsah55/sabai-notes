package com.cbs.sabainotes.utils;

public abstract class JwtUtils {
    public interface Headers {
        String BEARER_PREFIX = "Bearer";
        String AUTHORIZATION = "Authorization";
    }

    public static String getKey(){
        return ClaimFields.SECRET_KEY;
    }

    public interface ClaimFields{
        String USER_ID = "user_id";
        String  ROLES ="roles";
        String SECRET_KEY = "anythingrandomcanbesecretmakeitascomplexaspossible";
        String AUTHORIZATION_HEADER = "authorization";

    }
}
