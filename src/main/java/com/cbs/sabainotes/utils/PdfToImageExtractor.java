package com.cbs.sabainotes.utils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

public class PdfToImageExtractor {
    private static String IMAGE_FORMAT = "jpeg";
    private static String THUMBNAIL = "thumbnail";
    private static String COVER_IMAGE = "coverImage";

    public static byte[] extractCoverImage(MultipartFile file){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        try {
            PDDocument pdfDocument = PDDocument.load(convertMultiPartToFile(file));
            PDFRenderer pdfRenderer = new PDFRenderer(pdfDocument);
            BufferedImage coverImage = pdfRenderer.renderImage(0);
            ImageIO.write(coverImage,IMAGE_FORMAT,outputStream);
            inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            pdfDocument.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return createImage(inputStream,300,500);
    }

    public static byte[] generateThumbnail(MultipartFile file){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        InputStream inputStream = null;
        try {
            PDDocument pdfDocument = PDDocument.load(convertMultiPartToFile(file));
            PDFRenderer pdfRenderer = new PDFRenderer(pdfDocument);
            BufferedImage coverImage = pdfRenderer.renderImage(0);
            ImageIO.write(coverImage,IMAGE_FORMAT,outputStream);
            inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            pdfDocument.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return createImage(inputStream,100,150);

    }

    public static int getNoOfPagesFromFile(MultipartFile file) throws IOException {
        PDDocument pdfDocument = PDDocument.load(convertMultiPartToFile(file));
        return pdfDocument.getNumberOfPages();
    }


    private static byte[] createImage(InputStream inputStream, int height, int width){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        BufferedImage resizedImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        try {
            resizedImage.createGraphics().drawImage(ImageIO.read(inputStream).getScaledInstance(width,height, Image.SCALE_SMOOTH),0,0,null);
            ImageIO.write(resizedImage,"JPEG",outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream.toByteArray();
    }

    private static File convertMultiPartToFile(MultipartFile file ) throws IOException
    {
        File convFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        FileOutputStream fos = new FileOutputStream( convFile );
        fos.write( file.getBytes() );
        fos.close();
        return convFile;
    }
}
