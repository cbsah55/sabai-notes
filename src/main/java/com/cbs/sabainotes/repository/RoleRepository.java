package com.cbs.sabainotes.repository;

import com.cbs.sabainotes.entity.user.Role;
import com.cbs.sabainotes.entity.user.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Long> {


    Optional<Role> findByName(UserRole name);
}
