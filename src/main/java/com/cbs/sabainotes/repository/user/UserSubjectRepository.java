package com.cbs.sabainotes.repository.user;

import com.cbs.sabainotes.entity.courses.Subject;
import com.cbs.sabainotes.entity.user.User;
import com.cbs.sabainotes.entity.user.UserSubject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserSubjectRepository extends JpaRepository<UserSubject, Long> {
    Optional<UserSubject> findByUserAndSubject(User user, Subject subject);

    List<UserSubject> findAllByUser(User userFromContext);
}