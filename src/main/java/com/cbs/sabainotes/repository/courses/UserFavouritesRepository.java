package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.Material;
import com.cbs.sabainotes.entity.courses.UserFavourites;
import com.cbs.sabainotes.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserFavouritesRepository extends JpaRepository<UserFavourites, Long> {

    List<UserFavourites> findAllByUser(User user);

    Optional<UserFavourites> findByIdAndMaterial(long id, Material material);

    Optional<UserFavourites> findByUserAndMaterial(User user, Material material);

    boolean existsByUserAndMaterial(User user, Material material);
}