package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.RecentActivity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecentActivityRepository extends JpaRepository<RecentActivity, Long> {
}