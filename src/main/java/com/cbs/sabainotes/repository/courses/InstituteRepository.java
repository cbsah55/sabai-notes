package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.Institute;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InstituteRepository extends JpaRepository<Institute, Long> {
    List<Institute> findAllByNameContainingOrShortNameContaining(String filter,String shortNameFilter);
}