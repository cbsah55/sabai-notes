package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.Course;
import com.cbs.sabainotes.entity.courses.CourseFaculty;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseFacultyRepository extends JpaRepository<CourseFaculty, Long> {
    List<CourseFaculty> findAllByNameContaining(String filter);

    List<CourseFaculty> findAllByCourse(Course course);
}