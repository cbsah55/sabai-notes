package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.Material;
import com.cbs.sabainotes.entity.courses.MaterialTypeEnum;
import com.cbs.sabainotes.entity.courses.Subject;
import com.cbs.sabainotes.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MaterialRepository extends JpaRepository<Material, Long> {
    Material findByIdAndContributor(long id, User contributor);

    List<Material> findAllByContributor(User contributor);

    List<Material> findAllByVerified(boolean b);

    List<Material> findAllByNameContaining(String filter);

    List<Material> findAllBySubjectAndType(Subject subject, MaterialTypeEnum type);
}