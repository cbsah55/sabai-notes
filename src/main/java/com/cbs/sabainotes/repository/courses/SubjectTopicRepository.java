package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.SubjectTopic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectTopicRepository extends JpaRepository<SubjectTopic, Long> {
}