package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.AcademicSession;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcademicSessionsRepository extends JpaRepository<AcademicSession, Long> {
}