package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.CourseFaculty;
import com.cbs.sabainotes.entity.courses.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubjectRepository extends JpaRepository<Subject, Long> {
    boolean existsByName(String name);

    List<Subject> findByNameContainingOrShortNameContaining(String name, String shortName);

    List<Subject> findAllByFaculty(CourseFaculty courseFaculty);
}