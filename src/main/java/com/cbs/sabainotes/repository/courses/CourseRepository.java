package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.Course;
import com.cbs.sabainotes.entity.courses.FieldOfStudies;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {
    Optional<Course> findByName(String name);

    boolean existsByName(String name);

    List<Course> findAllByNameContaining(String filter);

    List<Course> findAllByFaculty(FieldOfStudies fieldOfStudies);
}