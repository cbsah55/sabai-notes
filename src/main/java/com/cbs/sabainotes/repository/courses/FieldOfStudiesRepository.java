package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.FieldOfStudies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FieldOfStudiesRepository extends JpaRepository<FieldOfStudies, Long> {
}