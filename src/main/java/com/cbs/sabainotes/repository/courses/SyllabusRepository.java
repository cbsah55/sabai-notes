package com.cbs.sabainotes.repository.courses;

import com.cbs.sabainotes.entity.courses.Syllabus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SyllabusRepository extends JpaRepository<Syllabus, Long> {
}