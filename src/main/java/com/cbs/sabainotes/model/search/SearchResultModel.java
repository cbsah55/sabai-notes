package com.cbs.sabainotes.model.search;

import lombok.Data;

@Data
public class SearchResultModel {

    private long id;
    private String name;
}
