package com.cbs.sabainotes.model;

import com.cbs.sabainotes.entity.user.Role;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Builder
public class UserModel {
    private Long id;
    private String fullName;
    private String email;
    private Set<Role> type;


}
