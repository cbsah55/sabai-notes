package com.cbs.sabainotes.model.courses;

import com.cbs.sabainotes.entity.courses.Course;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CourseFacultyModel {
    private Long id;
    private String name;
    private String shortName;
    private String slug;
    private Course course;
}
