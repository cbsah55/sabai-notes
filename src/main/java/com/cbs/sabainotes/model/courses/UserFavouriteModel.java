package com.cbs.sabainotes.model.courses;

import com.cbs.sabainotes.entity.courses.Material;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class UserFavouriteModel implements Serializable {

    private Long id;
    private Material material;
}
