package com.cbs.sabainotes.model.courses;

import com.cbs.sabainotes.entity.courses.FieldOfStudies;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CourseModel {

    private Long id;
    private String name;
    private String slug;
    private FieldOfStudies fieldOfStudy;
}
