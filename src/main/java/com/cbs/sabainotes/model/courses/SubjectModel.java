package com.cbs.sabainotes.model.courses;

import com.cbs.sabainotes.entity.courses.CourseFaculty;
import com.cbs.sabainotes.entity.courses.SubjectTopic;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class SubjectModel {

    private Long id;
    private String name;
    private String shortName;
    private String slug;
    private String tag;
    private CourseFaculty faculty;
    private List<SubjectTopic> subjectTopics;
}
