package com.cbs.sabainotes.model.courses;

import com.cbs.sabainotes.entity.courses.AcademicSessionEnum;
import com.cbs.sabainotes.entity.courses.Institute;
import com.cbs.sabainotes.entity.courses.Subject;
import com.cbs.sabainotes.model.UserModel;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
public class MaterialModel {

    private Long id;
    private String name;
    private String coverUrl; // cover of file uploaded can be url or image itself
    private String thumbUrl;
    private String description;
    private String url;// either url or file data itself
    private boolean handWritten;
    private boolean isPayPerMaterialEnabled;
    private Integer pages;
    private Integer price;
    private boolean isPrime;// if subscription is induced
    private Double rating;
    private String type; // note, video
    private boolean downloadable;
    private Integer downloadCount;
    private boolean printable;
    @JsonProperty(value = "isPublic")
    private boolean isPublic;
    private Integer viewCount;
    private boolean verified;
    private LocalDate uploadedDate;
    private UserModel contributedBy;
    private Institute institute;
    private Subject subject;
    private AcademicSessionEnum academicSession;
    private boolean owner;
    private boolean isFavourite;

    public Long getId() {
        return id;
    }

    public MaterialModel setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public MaterialModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public MaterialModel setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
        return this;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public MaterialModel setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public MaterialModel setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public MaterialModel setUrl(String url) {
        this.url = url;
        return this;
    }

    public boolean isHandWritten() {
        return handWritten;
    }

    public MaterialModel setHandWritten(boolean handWritten) {
        this.handWritten = handWritten;
        return this;
    }

    public boolean isPayPerMaterialEnabled() {
        return isPayPerMaterialEnabled;
    }

    public MaterialModel setPayPerMaterialEnabled(boolean payPerMaterialEnabled) {
        isPayPerMaterialEnabled = payPerMaterialEnabled;
        return this;
    }

    public Integer getPages() {
        return pages;
    }

    public MaterialModel setPages(Integer pages) {
        this.pages = pages;
        return this;
    }

    public Integer getPrice() {
        return price;
    }

    public MaterialModel setPrice(Integer price) {
        this.price = price;
        return this;
    }

    public boolean isPrime() {
        return isPrime;
    }

    public MaterialModel setPrime(boolean prime) {
        isPrime = prime;
        return this;
    }

    public Double getRating() {
        return rating;
    }

    public MaterialModel setRating(Double rating) {
        this.rating = rating;
        return this;
    }

    public String getType() {
        return type;
    }

    public MaterialModel setType(String type) {
        this.type = type;
        return this;
    }

    public boolean isDownloadable() {
        return downloadable;
    }

    public MaterialModel setDownloadable(boolean downloadable) {
        this.downloadable = downloadable;
        return this;
    }

    public Integer getDownloadCount() {
        return downloadCount;
    }

    public MaterialModel setDownloadCount(Integer downloadCount) {
        this.downloadCount = downloadCount;
        return this;
    }

    public boolean isPrintable() {
        return printable;
    }

    public MaterialModel setPrintable(boolean printable) {
        this.printable = printable;
        return this;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public MaterialModel setPublic(boolean aPublic) {
        isPublic = aPublic;
        return this;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public MaterialModel setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
        return this;
    }

    public boolean isVerified() {
        return verified;
    }

    public MaterialModel setVerified(boolean verified) {
        this.verified = verified;
        return this;
    }

    public LocalDate getUploadedDate() {
        return uploadedDate;
    }

    public MaterialModel setUploadedDate(LocalDate uploadedDate) {
        this.uploadedDate = uploadedDate;
        return this;
    }

    public UserModel getContributedBy() {
        return contributedBy;
    }

    public MaterialModel setContributedBy(UserModel contributedBy) {
        this.contributedBy = contributedBy;
        return this;
    }

    public Institute getInstitute() {
        return institute;
    }

    public MaterialModel setInstitute(Institute institute) {
        this.institute = institute;
        return this;
    }

    public Subject getSubject() {
        return subject;
    }

    public MaterialModel setSubject(Subject subject) {
        this.subject = subject;
        return this;
    }

    public AcademicSessionEnum getAcademicSession() {
        return academicSession;
    }

    public MaterialModel setAcademicSession(AcademicSessionEnum academicSession) {
        this.academicSession = academicSession;
        return this;
    }

    public boolean isOwner() {
        return owner;
    }

    public MaterialModel setOwner(boolean owner) {
        this.owner = owner;
        return this;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public MaterialModel setFavourite(boolean favourite) {
        isFavourite = favourite;
        return this;
    }
}
