package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;
import com.cbs.sabainotes.entity.user.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Materials are all the notes, syllabus, videos uploaded.
 */
@Table(name = "material")
@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class Material extends BaseEntity<Material> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @NotNull(message = "name cannot be null value")
    private String name;
    private String cover; // cover of file uploaded can be url or image itself
    private String thumbNail;
    private String description;
    private String url;// either url or file data itself
    private Boolean handWritten= false;
    private Boolean isPayPerMaterialEnabled =false;
    private Integer pages;
    private Integer price =0;
    private Boolean isPrime=false;// if subscription is induced
    private Double rating=0.0;
    @Enumerated(EnumType.STRING)
    private MaterialTypeEnum type; // note, video
    private Boolean downloadable=false;
    private Integer downloadCount=0;
    private Boolean printable=false;
    private Boolean isPublic=false;
    private Integer viewCount=0;
    private Boolean verified=false;
    private LocalDate uploadedDate;
    @Lob
    @Column(length = 10000000)
    private byte[] fileData;
    private String fileType;
    @Lob
    @Column(length = 10000000)
    private byte[] coverData;
    @Lob
    private byte[] thumbNailData;

    @OneToOne
    private User contributor;

    @OneToOne()
    private Subject subject;

    @OneToOne
    private Institute institute;

    @Enumerated(EnumType.STRING)
    private AcademicSessionEnum academicSession;


}