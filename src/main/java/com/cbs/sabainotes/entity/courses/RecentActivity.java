package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Table(name = "recent_activity")
@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class RecentActivity extends BaseEntity<RecentActivity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String contentName;
    private String lastReadItemId;
    private LocalDateTime lastReadTime;
    @OneToOne
    private Subject subject;

}