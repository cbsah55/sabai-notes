package com.cbs.sabainotes.entity.courses;

public enum MaterialTypeEnum {
    Note("Note"),
    Prev_Year_Question("Previous Year Question"),
    Prev_Year_Question_Solution("Previous Year Question Solution"),
    Syllabus("Syllabus"),
    Question_Bank("Question Bank"),
    Other("Other");

    private final String lable;


    MaterialTypeEnum(String lable) {
        this.lable = lable;
    }

    public String getLable() {
        return lable;
    }
}
