package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * Engineering
 * Computer Applications
 * Commerce
 * Arts
 * Law
 * Business Management
 */
@Entity
public class FieldOfStudies extends BaseEntity<FieldOfStudies> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @NotBlank(message = "name cannot be null or blank")
    private String name;

    public Long getId() {
        return id;
    }

    public FieldOfStudies setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public FieldOfStudies setName(String name) {
        this.name = name;
        return this;
    }

}
