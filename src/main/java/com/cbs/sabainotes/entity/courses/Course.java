package com.cbs.sabainotes.entity.courses;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * B.Tech/BE
 */
@Table(name = "Course")
@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String name;
    private String slug;

    @OneToOne
    @JoinColumn(referencedColumnName = "name")
    private FieldOfStudies faculty;

}
