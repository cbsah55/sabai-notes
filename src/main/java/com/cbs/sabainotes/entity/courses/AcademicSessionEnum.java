package com.cbs.sabainotes.entity.courses;

import lombok.Getter;

import java.util.List;

@Getter
public enum AcademicSessionEnum {
    firstSemester("First Semester","1st"),
    secondSemester("Second Semester","2nd"),
    thirdSemester("Third Semester","3rd"),
    fourthSemester("Fourth Semester","4th"),
    fifthSemester("Fifth Semester","5th"),
    sixthSemester("Sixth Semester","6th"),
    seventhSemester("Seventh Semester","7th"),
    eighthSemester("Eighth Semester","8th"),
    ninthSemester("Ninth Semester","9th"),
    tenthSemester("Tenth Semester","10th"),
    firstYear("First Year","1stFY"),
    secondYear("Second Year","2ndFY"),
    thirdYear("Third Year","3rdFY"),
    fourthYear("Fourth Year","4thFY"),
    fifthYear("Fifth Year","5thFY");

    private final String label;
    private final String code;

    AcademicSessionEnum(String label, String code) {
        this.label=label;
        this.code=code;
    }


    public static List<AcademicSessionEnum> getAllSemester(){
        return List.of(AcademicSessionEnum.firstSemester,
                AcademicSessionEnum.secondSemester,
                AcademicSessionEnum.thirdSemester,
                AcademicSessionEnum.fourthSemester,
                AcademicSessionEnum.fifthSemester,
                AcademicSessionEnum.sixthSemester,
                AcademicSessionEnum.seventhSemester,
                AcademicSessionEnum.eighthSemester,
                AcademicSessionEnum.ninthSemester,
                AcademicSessionEnum.tenthSemester
                );
    }

    public static List<AcademicSessionEnum> getALlYear(){
        return List.of(AcademicSessionEnum.firstYear,
                AcademicSessionEnum.secondYear,
                AcademicSessionEnum.thirdYear,
                AcademicSessionEnum.fourthYear,
                AcademicSessionEnum.fifthYear
                );
    }
}
