package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;
import java.util.Objects;

@Table(name = "subject")
@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class Subject extends BaseEntity<Subject> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String shortName;
    private String slug;
    private String tag;

    @OneToOne
    private CourseFaculty faculty;

    @OneToMany(mappedBy = "subject" ,cascade = CascadeType.ALL)
    private List<SubjectTopic> subjectTopics;

    public Subject() {

    }

    /**
     * Multemedia and virtual reality MAVR, tag - multemedia-and-virtual-reality
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Subject subject = (Subject) o;

        return Objects.equals(id, subject.id);
    }

    @Override
    public int hashCode() {
        return 1313511265;
    }
}