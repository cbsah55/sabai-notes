package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "subject_topics")
@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class SubjectTopic extends BaseEntity<SubjectTopic> {

    public SubjectTopic() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String name;


    @ManyToOne(fetch = FetchType.LAZY)
    private Subject subject;



    /**
     * Concept-of-multemedia
     * data-compression
     */

}