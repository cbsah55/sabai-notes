package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Table(name = "syllabus")
@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class Syllabus extends BaseEntity<Syllabus> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate year;
    private Boolean active;
    private Boolean approved;

    @OneToOne
    private Course course;

    @OneToOne
    private Institute institute;

    @OneToOne
    private AcademicSession semester;

    @OneToOne
    private CourseFaculty courseFaculty;

    @OneToOne
    private Subject subject;

}