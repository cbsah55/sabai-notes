package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class CourseFaculty extends BaseEntity<CourseFaculty> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
    private String shortName;
    private String slug;

    @OneToOne(cascade = CascadeType.ALL)
    private Course course;

    public CourseFaculty() {

    }


    /**
     * marine engineering , course - btech
     * civil engineering
     * Electrical engineering
     * electronics and communications engineering
     * Information Engineering
     * Computer science Engineering
     */

}