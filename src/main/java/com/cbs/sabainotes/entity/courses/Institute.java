package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Table(name = "institute")
@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class Institute extends BaseEntity<Institute> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "name of institute cannot be null or blank.")
    private String name;
    private String shortName;
    private String slug;


    /**
     * Purwanchan University PU
     */

}