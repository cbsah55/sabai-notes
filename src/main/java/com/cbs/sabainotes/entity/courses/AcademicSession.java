package com.cbs.sabainotes.entity.courses;

import com.cbs.sabainotes.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;

@Table(name = "academic_sessions")
@Entity
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class AcademicSession extends BaseEntity<AcademicSession> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Max(value = 255,message = "name cannot be more than 255 characters.")
    private String name;
    private String slug;

    /**
     * 1st semester
     * 2nd semester
     * 3rd semester
     * 4th semester
     * 5th semester
     * 6th semester
     * 7th semester
     *
     */

}