package com.cbs.sabainotes.entity.user;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum UserRole {
    ROLE_ADMIN,ROLE_MOD,ROLE_USER
}
