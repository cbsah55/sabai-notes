package com.cbs.sabainotes.entity.user;

import com.cbs.sabainotes.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class User extends BaseEntity<User> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    private String phone;


    private String password;

    private String avatar;
    private Boolean enabled;


    @OneToMany(fetch = FetchType.EAGER,mappedBy = "user")
    private List<Role> roles;

   @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<UserSubject> mySubjects;

    @JsonIgnore
    public String getFullName(){
        return this.firstName +" "+ this.lastName;
    }
}
