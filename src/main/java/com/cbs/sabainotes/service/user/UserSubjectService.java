package com.cbs.sabainotes.service.user;

import com.cbs.sabainotes.model.courses.SubjectModel;

import java.util.List;

public interface UserSubjectService {

    void addUserSubject(long subjectId);

    void deleteUserSubject(long subjectId);

    List<SubjectModel> getUserSubjects();
}
