package com.cbs.sabainotes.service.user;

import com.cbs.sabainotes.entity.user.User;

import java.util.List;

public interface UserService {
    User save(User user);
    User getById(long id);

    User getByEmail(String email);

    boolean existsByEmail(String email);

    boolean existsById(long id);

    List<User> getAll();

    boolean isFirstUser();
}
