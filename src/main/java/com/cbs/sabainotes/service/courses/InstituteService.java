package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.entity.courses.Institute;

import java.util.List;

public interface InstituteService {

    Institute create(Institute institute);

    Institute update(Institute institute);

    Institute getById(long id);

    void deleteById(long id);

    List<Institute> getALl();
}
