package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.dto.courses.SubjectDto;
import com.cbs.sabainotes.entity.courses.Subject;
import com.cbs.sabainotes.exception.BadRequestException;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.mappers.SubjectMapper;
import com.cbs.sabainotes.model.courses.SubjectModel;
import com.cbs.sabainotes.repository.courses.CourseFacultyRepository;
import com.cbs.sabainotes.repository.courses.SubjectRepository;
import com.cbs.sabainotes.service.user.UserService;
import com.cbs.sabainotes.utils.LocalStringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SubjectServiceImpl {

    private final SubjectRepository subjectRepository;
    private final CourseFacultyRepository courseFacultyRepository;
    private final UserService userService;

    public SubjectModel create(SubjectDto subjectDto){
        sameNameAlreadyExists(subjectDto);
        var courseFaculty = courseFacultyRepository.findById(subjectDto.getFacultyId())
                .orElseThrow(() -> new NotFoundException(String.format("Course not found with id",subjectDto.getFacultyId())));
        var subjectEntity = SubjectMapper.dtoToEntity(subjectDto);
        subjectEntity.setFaculty(courseFaculty);

        return SubjectMapper.entityToModel(subjectRepository.save(subjectEntity));
    }


    public SubjectModel update(SubjectDto subjectDto){
        sameNameAlreadyExists(subjectDto);
        var subject = getById(subjectDto.getId());
        subject.setName(subjectDto.getName());
        subject.setTag(LocalStringUtils.createTag(subjectDto.getTag()));
        subject.setSlug(LocalStringUtils.createSlug(subject.getName()));
        subject.setShortName(subjectDto.getShortName());

        return SubjectMapper.entityToModel(subjectRepository.save(subject));
    }

    public SubjectModel getModelById(Long id) {
        return SubjectMapper.entityToModel(getById(id));
    }

    public List<SubjectModel> getModels(){
        return subjectRepository.findAll().stream().map(SubjectMapper::entityToModel).collect(Collectors.toList());
    }

    private void sameNameAlreadyExists(SubjectDto subjectDto) {
        if (existsByName(subjectDto.getName())) {
            throw new BadRequestException("Subject already exists with same name.");
        }
    }

    public void deleteById(long id){
        var subject = getById(id);
        subjectRepository.delete(subject);
    }


    public Subject getById(long id){
        return subjectRepository.findById(id)
                .orElseThrow(()-> new NotFoundException(String.format("Subject with id %d not found",id)));
    }

    public boolean existsByName(String name ){
        return subjectRepository.existsByName(name);
    }








}
