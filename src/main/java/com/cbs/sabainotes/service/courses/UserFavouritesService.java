package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.entity.courses.Material;
import com.cbs.sabainotes.model.courses.UserFavouriteModel;

import java.util.List;

public interface UserFavouritesService {

    void create(long materialId);

    void deleteByMaterialId(long materialId);

    List<UserFavouriteModel> getALlByUser();

    boolean existsByUserIdAndMaterial(long userId, Material material);
}
