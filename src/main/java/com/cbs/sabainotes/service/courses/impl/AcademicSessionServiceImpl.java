package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.entity.courses.AcademicSession;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.repository.courses.AcademicSessionsRepository;
import com.cbs.sabainotes.service.courses.AcademicSessionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AcademicSessionServiceImpl implements AcademicSessionsService {

    private final AcademicSessionsRepository academicSessionsRepository;


    @Override
    public AcademicSession create(AcademicSession dto) {
        AcademicSession academicSession = new AcademicSession();
        academicSession.setName(dto.getName());
        academicSession.setSlug(dto.getSlug());
        return academicSessionsRepository.save(academicSession);
    }

    @Override
    public AcademicSession update(AcademicSession academicSession) {
        return null;
    }

    @Override
    public AcademicSession getById(long id) {
        return academicSessionsRepository.findById(id)
                .orElseThrow(()->new NotFoundException(String.format("Academic Session with id %d not found",id)));
    }

    @Override
    public void deleteById(long id) {
        var entity = getById(id);
        academicSessionsRepository.delete(entity);
    }

    @Override
    public List<AcademicSession> getALl() {
        return academicSessionsRepository.findAll();
    }
}
