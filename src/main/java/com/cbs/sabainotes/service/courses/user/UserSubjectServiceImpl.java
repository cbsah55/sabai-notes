package com.cbs.sabainotes.service.courses.user;

import com.cbs.sabainotes.entity.user.User;
import com.cbs.sabainotes.entity.user.UserSubject;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.mappers.SubjectMapper;
import com.cbs.sabainotes.model.courses.SubjectModel;
import com.cbs.sabainotes.repository.user.UserSubjectRepository;
import com.cbs.sabainotes.service.courses.impl.SubjectServiceImpl;
import com.cbs.sabainotes.service.user.UserService;
import com.cbs.sabainotes.service.user.UserSubjectService;
import com.cbs.sabainotes.utils.RequestContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserSubjectServiceImpl implements UserSubjectService {


    private final UserService userService;
    private final SubjectServiceImpl subjectService;
    private final UserSubjectRepository userSubjectRepository;


    @Override
    public void addUserSubject(long subjectId) {
        var subject = subjectService.getById(subjectId);
        var user = getUserFromContext();
        var userSubject = new UserSubject();
        userSubject.setUser(user);
        userSubject.setSubject(subject);
        userSubjectRepository.save(userSubject);
    }

    private User getUserFromContext() {
        return userService.getById(RequestContext.getUserId());
    }

    @Override
    public void deleteUserSubject(long subjectId) {
        var user = userService.getById(getUserFromContext().getId());
        var userSubject = userSubjectRepository.findByUserAndSubject(user, subjectService.getById(subjectId))
                        .orElseThrow(()-> new NotFoundException("Your saved subject is not foung"));
        userSubjectRepository.delete(userSubject);
    }

    @Override
    public List<SubjectModel> getUserSubjects() {
        var userSubjects = userSubjectRepository.findAllByUser(getUserFromContext());
        return userSubjects.stream()
                .map(UserSubject::getSubject)
                .map(SubjectMapper::entityToModel)
                .collect(Collectors.toList());
    }
}
