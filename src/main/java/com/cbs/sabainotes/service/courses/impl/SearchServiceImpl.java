package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.entity.courses.Course;
import com.cbs.sabainotes.entity.courses.CourseFaculty;
import com.cbs.sabainotes.entity.courses.FieldOfStudies;
import com.cbs.sabainotes.entity.courses.Institute;
import com.cbs.sabainotes.entity.courses.Material;
import com.cbs.sabainotes.entity.courses.Subject;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.model.search.SearchResultModel;
import com.cbs.sabainotes.repository.courses.CourseFacultyRepository;
import com.cbs.sabainotes.repository.courses.CourseRepository;
import com.cbs.sabainotes.repository.courses.InstituteRepository;
import com.cbs.sabainotes.repository.courses.MaterialRepository;
import com.cbs.sabainotes.repository.courses.SubjectRepository;
import com.cbs.sabainotes.service.courses.FieldOfStudyService;
import com.cbs.sabainotes.service.courses.SearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final SubjectRepository subjectRepository;
    private final CourseRepository courseRepository;
    private final InstituteRepository instituteRepository;
    private final CourseFacultyRepository courseFacultiesRepository;
    private final MaterialRepository materialRepository;
    private final FieldOfStudyService fieldOfStudyService;




    @Override
    public List<SearchResultModel> getAllSubjectsByFilter(String filter) {
        return subjectRepository.findByNameContainingOrShortNameContaining(filter,filter)
                .stream().map(this::subjectToResultModel)
                .collect(Collectors.toList());
    }


    @Override
    public List<SearchResultModel> getAllCoursesByFilter(String filter) {

        return courseRepository.findAllByNameContaining(filter).
                stream().map(this::courseToResultModel)
                .collect(Collectors.toList());
    }

    @Override
    public List<SearchResultModel> getALlCourses(long fieldOfStudiesId) {
        var fieldOfStudies = fieldOfStudyService.getById(fieldOfStudiesId);
        return courseRepository.findAllByFaculty(fieldOfStudies).
                stream().map(this::courseToResultModel)
                .collect(Collectors.toList());
    }

    @Override
    public List<SearchResultModel> getALlCourseFaculty(long courseId) {
        var course = courseRepository.findById(courseId).orElseThrow(()->new NotFoundException("COurse not found"));
        return courseFacultiesRepository.findAllByCourse(course)
                .stream().map(this::facultyToResultModel)
                .collect(Collectors.toList());
    }

    @Override
    public List<SearchResultModel> getAllInstitutesByFilter(String filter) {
        return instituteRepository.findAllByNameContainingOrShortNameContaining(filter,filter)
                .stream().map(this::instituteToResultModel)
                .collect(Collectors.toList());
    }

    @Override
    public List<SearchResultModel> getAllFacultiesByFilter(String filter) {
        return courseFacultiesRepository.findAllByNameContaining(filter)
                .stream().map(this::facultyToResultModel)
                .collect(Collectors.toList());
    }

    @Override
    public List<SearchResultModel> getAllMaterialsByFilter(String filter) {
        return materialRepository.findAllByNameContaining(filter)
                .stream().map(this::materialToResultModel)
                .collect(Collectors.toList());
    }
    @Override
    public List<SearchResultModel> getALlFieldOfStudies() {
        return fieldOfStudyService.getAll()
                .stream().map(this::fieldOfStudyToResultModel)
                .collect(Collectors.toList());
    }

    @Override
    public List<SearchResultModel> getALlSubjectsByCourseFaculty(long courseFacultyId) {
        var courseFaculty = courseFacultiesRepository.findById(courseFacultyId)
                .orElseThrow(()-> new NotFoundException("Course Faculty not found"));
        return subjectRepository.findAllByFaculty(courseFaculty)
                .stream()
                .map(this::subjectToResultModel)
                .collect(Collectors.toList());
    }

    private  SearchResultModel fieldOfStudyToResultModel(FieldOfStudies fieldOfStudies) {
        SearchResultModel searchResultModel = new SearchResultModel();
        searchResultModel.setName(fieldOfStudies.getName());
        searchResultModel.setId(fieldOfStudies.getId());
        return searchResultModel;
    }

    private  SearchResultModel materialToResultModel(Material material) {
        SearchResultModel searchResultModel = new SearchResultModel();
        searchResultModel.setName(material.getName() + " -"+ material.getType().name());
        searchResultModel.setId(material.getId());
        return searchResultModel;
    }

    private  SearchResultModel facultyToResultModel(CourseFaculty courseFaculty) {
        SearchResultModel searchResultModel = new SearchResultModel();
        searchResultModel.setName(courseFaculty.getName() + " -"+ courseFaculty.getCourse().getName());
        searchResultModel.setId(courseFaculty.getId());
        return searchResultModel;
    }

    private  SearchResultModel instituteToResultModel(Institute institute) {
        SearchResultModel searchResultModel = new SearchResultModel();
        searchResultModel.setName(institute.getName());
        searchResultModel.setId(institute.getId());
        return searchResultModel;
    }

    private SearchResultModel subjectToResultModel(Subject subject){
        SearchResultModel searchResultModel = new SearchResultModel();
        searchResultModel.setName(subject.getName());
        searchResultModel.setId(subject.getId());
        return searchResultModel;
    }

    private SearchResultModel courseToResultModel(Course course){
        SearchResultModel searchResultModel = new SearchResultModel();
        searchResultModel.setName(course.getName());
        searchResultModel.setId(course.getId());
        return searchResultModel;
    }
}
