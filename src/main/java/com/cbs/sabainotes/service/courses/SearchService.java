package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.entity.courses.AcademicSessionEnum;
import com.cbs.sabainotes.entity.courses.MaterialTypeEnum;
import com.cbs.sabainotes.model.search.SearchResultModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface SearchService {
    List<SearchResultModel> getAllSubjectsByFilter(String filter);

    List<SearchResultModel> getAllCoursesByFilter(String filter);

    List<SearchResultModel> getAllInstitutesByFilter(String filter);

    List<SearchResultModel> getAllFacultiesByFilter(String filter);

    default List<Map<String,Object>> getALlAcademicSessions(){
        List<Map<String,Object>> academicSessionsDetails = new ArrayList<>();
        AcademicSessionEnum[] sessionEnums = AcademicSessionEnum.values();
        for (AcademicSessionEnum sessionEnum : sessionEnums) {
            Map<String,Object> sesisons = new HashMap<>();
            sesisons.put("name",sessionEnum.getLabel());
            sesisons.put("id",sessionEnum);
            academicSessionsDetails.add(sesisons);
        }
        return academicSessionsDetails;
    }

    default List<Map<String,Object>> getMaterialTypes(){
        List<Map<String,Object>> materialTypes = new ArrayList<>();
        MaterialTypeEnum[] enums = MaterialTypeEnum.values();
        for (MaterialTypeEnum materialTypeEnum : enums) {
            Map<String,Object> type = new HashMap<>();
            type.put("id",materialTypeEnum);
            type.put("name",materialTypeEnum.getLable());
            materialTypes.add(type);
        }
        return materialTypes;
    }

    List<SearchResultModel> getAllMaterialsByFilter(String filter);

    List<SearchResultModel> getALlFieldOfStudies();

    List<SearchResultModel> getALlCourses(long fieldOfStudiesId);

    List<SearchResultModel> getALlCourseFaculty(long courseId);

    List<SearchResultModel> getALlSubjectsByCourseFaculty(long courseFacultyId);
}
