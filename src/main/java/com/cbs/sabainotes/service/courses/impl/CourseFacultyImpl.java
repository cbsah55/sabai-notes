package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.dto.courses.CourseFacultyDto;
import com.cbs.sabainotes.entity.courses.Course;
import com.cbs.sabainotes.entity.courses.CourseFaculty;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.model.courses.CourseFacultyModel;
import com.cbs.sabainotes.repository.courses.CourseFacultyRepository;
import com.cbs.sabainotes.repository.courses.CourseRepository;
import com.cbs.sabainotes.service.courses.CourseFacultyService;
import com.cbs.sabainotes.utils.LocalStringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CourseFacultyImpl implements CourseFacultyService {

    private final CourseFacultyRepository courseFacultyRepository;
    private final CourseRepository courseRepository;

    @Override
    public CourseFacultyModel create(CourseFacultyDto courseFacultyDto) {
        Course course = getCourse(courseFacultyDto.getCourseId());
        var courseFaculty = dtoToEntity(courseFacultyDto);
        courseFaculty.setCourse(course);
        return entityToModel(courseFacultyRepository.save(courseFaculty));
    }



    @Override
    public CourseFacultyModel update(CourseFacultyDto courseFacultyDto) {
        getCourse(courseFacultyDto.getCourseId());
        var courseFaculty = getById(courseFacultyDto.getId());
        courseFaculty.setName(courseFacultyDto.getName());
        courseFaculty.setShortName(courseFacultyDto.getShortName());
        courseFaculty.setSlug(LocalStringUtils.createSlug(courseFacultyDto.getName()));
        return entityToModel(courseFacultyRepository.save(courseFaculty));
    }

    @Override
    public void deleteById(long id) {
        var courseFaculty = getById(id);
        courseFacultyRepository.delete(courseFaculty);

    }

    public CourseFaculty getById(long id){
        return courseFacultyRepository.findById(id)
                .orElseThrow(()-> new NotFoundException(String.format("CourseFaculty with id %d not found.",id)));
    }

    @Override
    public CourseFacultyModel getModelById(long id) {
        return entityToModel(getById(id));
    }

    @Override
    public List<CourseFacultyModel> getModels() {
        return courseFacultyRepository.findAll().stream().map(this::entityToModel).collect(Collectors.toList());
    }

    private Course getCourse(long courseId) {
        return courseRepository.findById(courseId)
                .orElseThrow(()-> new NotFoundException(String.format("Course with id %d not found", courseId)));
    }

    private CourseFaculty dtoToEntity(CourseFacultyDto dto) {
        CourseFaculty courseFaculty = new CourseFaculty();
        courseFaculty.setId(dto.getId());
        courseFaculty.setName(dto.getName());
        courseFaculty.setShortName(dto.getShortName());
        courseFaculty.setSlug(LocalStringUtils.createSlug(dto.getName()));
        return courseFaculty;
    }

    private CourseFacultyModel entityToModel(CourseFaculty courseFaculty){
        return CourseFacultyModel.builder()
                .id(courseFaculty.getId())
                .name(courseFaculty.getName())
                .shortName(courseFaculty.getShortName())
                .slug(courseFaculty.getSlug())
                .course(courseFaculty.getCourse())
                .build();
    }
}
