package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.dto.courses.MaterialDto;
import com.cbs.sabainotes.entity.courses.Material;
import com.cbs.sabainotes.entity.courses.MaterialTypeEnum;
import com.cbs.sabainotes.model.courses.MaterialModel;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface MaterialService {

    Material getById(long id);

    MaterialModel getModelById(long id);

    MaterialModel create(MaterialDto materialDto);

    MaterialModel upload(MultipartFile file) throws IOException;

    MaterialModel update(MaterialDto materialDto);

    List<MaterialModel> getAllContributorMaterials();

    MaterialModel getModelByIdAndUserId(long id, long userId);

    void deleteByUserIdAndMaterialID(long userId, long materialId);

    MaterialModel verifyMaterial(long id);

    void deleteById(long materialId);

    List<MaterialModel> getAllVerified(Boolean verified);


    void updateViewCount(Material material);

    void updateMaterialRating(long id, double value);

    List<MaterialModel> getAllBySubjectAndType(long subjectId, MaterialTypeEnum type);
}
