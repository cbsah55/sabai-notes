package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.dto.courses.SubjectTopicDto;
import com.cbs.sabainotes.entity.courses.SubjectTopic;

import java.util.List;

public interface SubjectTopicService {

    SubjectTopic create(SubjectTopicDto subjectTopic);

    SubjectTopic update(SubjectTopicDto subjectTopic);

    SubjectTopic getById(long id);

    void deleteById(long id);

    List<SubjectTopic> getALl();
}
