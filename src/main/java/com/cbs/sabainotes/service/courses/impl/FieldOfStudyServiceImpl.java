package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.entity.courses.FieldOfStudies;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.repository.courses.FieldOfStudiesRepository;
import com.cbs.sabainotes.service.courses.FieldOfStudyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FieldOfStudyServiceImpl implements FieldOfStudyService {

    private final FieldOfStudiesRepository fieldOfStudiesRepository;

    @Override
    public FieldOfStudies create(FieldOfStudies fieldOfStudies) {
        return fieldOfStudiesRepository.save(fieldOfStudies);
    }

    @Override
    public void deleteById(long id) {
        fieldOfStudiesRepository.delete(getById(id));
    }

    @Override
    public FieldOfStudies getById(long id) {
        return fieldOfStudiesRepository.findById(id).
                orElseThrow(() -> new NotFoundException(String.format("FieldOfStudy wit id %d not found.",id)));
    }

    @Override
    public List<FieldOfStudies> getAll() {
        return fieldOfStudiesRepository.findAll();
    }
}
