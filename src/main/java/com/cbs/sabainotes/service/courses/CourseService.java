package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.dto.courses.CourseDto;
import com.cbs.sabainotes.model.courses.CourseModel;

import java.util.List;

public interface CourseService {
    CourseModel create(CourseDto courseDto);

    CourseModel update(CourseDto courseDto);

    void deleteById(long id);

    CourseModel getModelById(long id);

    List<CourseModel> getModels();
}
