package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.dto.courses.SubjectTopicDto;
import com.cbs.sabainotes.entity.courses.SubjectTopic;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.repository.courses.SubjectTopicRepository;
import com.cbs.sabainotes.service.courses.SubjectTopicService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SubjectTopicServiceImpl implements SubjectTopicService {

    private final SubjectTopicRepository subjectTopicRepository;
    private final SubjectServiceImpl subjectService;

    @Override
    public SubjectTopic create(SubjectTopicDto dto) {
        var subject =subjectService.getById(dto.getSubjectId());
        SubjectTopic subjectTopic = new SubjectTopic();
        subjectTopic.setName(dto.getName());
        subjectTopic.setSubject(subject);
        return subjectTopicRepository.save(subjectTopic);
    }

    @Override
    public SubjectTopic update(SubjectTopicDto dto) {
        var subject =subjectService.getById(dto.getSubjectId());
        var subjectTopic = getById(dto.getId());
        subjectTopic.setName(dto.getName());
        subjectTopic.setSubject(subject);
        return subjectTopicRepository.save(subjectTopic);
    }

    @Override
    public SubjectTopic getById(long id) {
        return subjectTopicRepository.findById(id)
                .orElseThrow(()-> new NotFoundException(String.format("Subject topic with id %d not found",id)));
    }

    @Override
    public void deleteById(long id) {
        var subjectTopic = getById(id);
        subjectTopicRepository.delete(subjectTopic);

    }

    @Override
    public List<SubjectTopic> getALl() {
        return null;
    }
}
