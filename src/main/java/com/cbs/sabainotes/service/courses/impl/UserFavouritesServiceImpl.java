package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.entity.courses.Material;
import com.cbs.sabainotes.entity.courses.UserFavourites;
import com.cbs.sabainotes.entity.user.User;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.model.courses.UserFavouriteModel;
import com.cbs.sabainotes.repository.courses.UserFavouritesRepository;
import com.cbs.sabainotes.service.courses.MaterialService;
import com.cbs.sabainotes.service.courses.UserFavouritesService;
import com.cbs.sabainotes.service.user.UserService;
import com.cbs.sabainotes.utils.RequestContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserFavouritesServiceImpl implements UserFavouritesService {

    private final UserFavouritesRepository userFavouritesRepository;
    private final UserService userService;
    private final MaterialService materialService;

    @Override
    public void create(long materialId) {
        //TODO get id from jwt from request headers.
      var user = userService.getById(RequestContext.getUserId());
       var material = materialService.getById(materialId);
       UserFavourites userFavourites = new UserFavourites();
       userFavourites.setUser(user);
       userFavourites.setMaterial(material);
       userFavouritesRepository.save(userFavourites);
    }

    @Override
    public void deleteByMaterialId(long materialId) {
        var material = materialService.getById(materialId);
        var user = userService.getById(RequestContext.getUserId());
        var userFav = userFavouritesRepository.findByUserAndMaterial(user,material)
                .orElseThrow(()-> new NotFoundException(String.format("Favourite not found with id %d",materialId)));
        userFavouritesRepository.delete(userFav);
    }

    @Override
    public List<UserFavouriteModel> getALlByUser() {
        var user = userService.getById(RequestContext.getUserId());
        return userFavouritesRepository.findAllByUser(user).stream().map(this::entityToModel).collect(Collectors.toList());
    }

    @Override
    public boolean existsByUserIdAndMaterial(long userId, Material material) {
        var user = userService.getById(RequestContext.getUserId());
        return userFavouritesRepository.existsByUserAndMaterial(user,material);
    }

    private User getUser(long userId){
        return userService.getById(userId);
    }

    private UserFavouriteModel entityToModel(UserFavourites userFavourites) {
        UserFavouriteModel userFavouriteModel = new UserFavouriteModel();
        userFavouriteModel.setId(userFavourites.getId());
        userFavouriteModel.setMaterial(userFavourites.getMaterial());

        return userFavouriteModel;
    }
}
