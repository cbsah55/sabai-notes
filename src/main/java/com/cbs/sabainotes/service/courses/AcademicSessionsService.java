package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.entity.courses.AcademicSession;

import java.util.List;

public interface AcademicSessionsService {

    AcademicSession create(AcademicSession academicSession);
    AcademicSession update(AcademicSession academicSession);
    AcademicSession getById(long id);
    void deleteById(long id);

    List<AcademicSession> getALl();



}
