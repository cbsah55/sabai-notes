package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.dto.courses.CourseFacultyDto;
import com.cbs.sabainotes.model.courses.CourseFacultyModel;

import java.util.List;

public interface CourseFacultyService {

    CourseFacultyModel create(CourseFacultyDto courseFacultyDto);

    CourseFacultyModel update(CourseFacultyDto courseFacultyDto);

    void deleteById(long id);

    CourseFacultyModel getModelById(long id);

    List<CourseFacultyModel> getModels();
}
