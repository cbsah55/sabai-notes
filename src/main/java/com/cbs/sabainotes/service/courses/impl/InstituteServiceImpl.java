package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.entity.courses.Institute;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.repository.courses.InstituteRepository;
import com.cbs.sabainotes.service.courses.InstituteService;
import com.cbs.sabainotes.utils.LocalStringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class InstituteServiceImpl implements InstituteService {

    private final InstituteRepository instituteRepository;

    @Override
    public Institute create(Institute institute) {
        institute.setSlug(LocalStringUtils.createSlug(institute.getName()));
        return instituteRepository.save(institute);
    }

    @Override
    public Institute update(Institute institute) {
        var instituteEntity = getById(institute.getId());
        instituteEntity.setName(institute.getName());
        instituteEntity.setShortName(institute.getShortName());
        instituteEntity.setSlug(LocalStringUtils.createSlug(institute.getName()));
        return instituteRepository.save(instituteEntity);
    }

    @Override
    public Institute getById(long id) {
        return instituteRepository.findById(id)
                .orElseThrow(()-> new NotFoundException(String.format("Institute with id %d not found.",id)));
    }

    @Override
    public void deleteById(long id) {
        var institute = getById(id);
        instituteRepository.delete(institute);
    }

    @Override
    public List<Institute> getALl() {
        return instituteRepository.findAll();
    }
}
