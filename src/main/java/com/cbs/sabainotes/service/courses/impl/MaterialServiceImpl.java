package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.constants.APIErrors;
import com.cbs.sabainotes.controller.publicController.PublicMaterialController;
import com.cbs.sabainotes.dto.courses.MaterialDto;
import com.cbs.sabainotes.entity.courses.Material;
import com.cbs.sabainotes.entity.courses.MaterialTypeEnum;
import com.cbs.sabainotes.entity.user.User;
import com.cbs.sabainotes.exception.BadRequestException;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.model.UserModel;
import com.cbs.sabainotes.model.courses.MaterialModel;
import com.cbs.sabainotes.repository.courses.MaterialRepository;
import com.cbs.sabainotes.repository.courses.UserFavouritesRepository;
import com.cbs.sabainotes.service.courses.InstituteService;
import com.cbs.sabainotes.service.courses.MaterialService;
import com.cbs.sabainotes.service.user.UserService;
import com.cbs.sabainotes.utils.LocalStringUtils;
import com.cbs.sabainotes.utils.PdfToImageExtractor;
import com.cbs.sabainotes.utils.RequestContext;
import io.jsonwebtoken.lang.Assert;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MaterialServiceImpl implements MaterialService {
    private static String THUMBNAIL = "thumbnail";
    private static String COVER_IMAGE = "coverImage";
    private final MaterialRepository materialRepository;
    private final UserService userService;
    private final SubjectServiceImpl subjectService;
    private final InstituteService instituteService;
    private final UserFavouritesRepository userFavouritesRepository;


    @Override
    public MaterialModel upload(MultipartFile file) throws IOException {
            checkDocumentValidation(file);
        User user = getUser();
        Material material = new Material();

            material.setFileData(file.getBytes());
            material.setFileType(file.getContentType());
            material.setContributor(user);
            material.setCoverData(PdfToImageExtractor.extractCoverImage(file));
            material.setThumbNailData(PdfToImageExtractor.generateThumbnail(file));
            material.setName(LocalStringUtils.getProcessedFileName(file.getOriginalFilename(),user.getFullName()));
            material.setUploadedDate(LocalDate.now());
            material.setPages(PdfToImageExtractor.getNoOfPagesFromFile(file));
            material.setType(MaterialTypeEnum.Note);

        return entityToModel(materialRepository.save(material));

    }

    @Override
    public MaterialModel update(MaterialDto materialDto) {
        var contributorId = RequestContext.getUserId();
        var material = getByIdAndUserId(materialDto.getId(),contributorId);

        if (Objects.nonNull(materialDto.getSubjectId())){
            var subject = subjectService.getById(materialDto.getSubjectId());
            material.setSubject(subject);
        }
        if (Objects.nonNull(materialDto.getInstituteId())){
            var institute = instituteService.getById(materialDto.getInstituteId());
            material.setInstitute(institute);

        }
       material.setName(materialDto.getName());
       material.setDescription(materialDto.getDescription());
       material.setHandWritten(materialDto.isHandWritten());
       material.setDownloadable(materialDto.isDownloadable());
       material.setPrintable(materialDto.isPrintable());
       material.setIsPublic(materialDto.isPublic());
       material.setType(materialDto.getType());
       material.setAcademicSession(materialDto.getAcademicSession());
        return entityToModel(materialRepository.save(material));
    }

    @Override
    public Material getById(long id){
        return materialRepository.findById(id)
                .orElseThrow(()-> new NotFoundException(String.format("Material with id %d not found",id)));
    }

    @Override
    public MaterialModel getModelById(long id){
        return materialRepository.findById(id).map(this::entityToModel)
                .orElseThrow(()-> new NotFoundException(String.format("Material with id %d not found",id)));
    }
    @Override
    public MaterialModel create(MaterialDto materialDto) {
        return null;
    }



    private Material getByIdAndUserId(long id, long userId){
        var user = userService.getById(userId);
        return materialRepository.findByIdAndContributor(id,user);
    }

    @Override
    public List<MaterialModel> getAllContributorMaterials(){
        User contributor = getUser();
        return materialRepository.findAllByContributor(contributor).stream().map(this::entityToModel).collect(Collectors.toList());

    }

    private User getUser() {
        return userService.getById(RequestContext.getUserId());
    }

    @Override
    public MaterialModel getModelByIdAndUserId(long id,long userId){
        User contributor = getUser();
        return entityToModel(materialRepository.findByIdAndContributor(id, contributor));
    }

    @Override
    public void deleteByUserIdAndMaterialID(long userId, long materialId) {
            var material = getByIdAndUserId(materialId,userId);
            materialRepository.delete(material);

    }

    @Override
    public void deleteById(long materialId) {
            var material = getById(materialId);
            materialRepository.delete(material);
    }

    @Override
    public MaterialModel verifyMaterial(long id) {
        var material = getById(id);
        material.setVerified(true);
       return entityToModel(materialRepository.save(material));
    }

    @Override
    public List<MaterialModel> getAllVerified(Boolean verified) {
        return materialRepository.findAllByVerified(true).stream().map(this::entityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public void updateViewCount(Material material) {
        material.setViewCount(material.getViewCount()+1);
        materialRepository.save(material);
    }

    @Override
    public void updateMaterialRating(long id, double value) {
        var material = getById(id);
        material.setRating(value);
        materialRepository.save(material);
    }

    @Override
    public List<MaterialModel> getAllBySubjectAndType(long subjectId, MaterialTypeEnum type) {
        var subject = subjectService.getById(subjectId);
        return materialRepository.findAllBySubjectAndType(subject,type)
                .stream()
                .map(this::entityToModel)
                .collect(Collectors.toList());
    }

    private MaterialModel entityToModel(Material material) {
        MaterialModel materialModel = new MaterialModel()
                .setId(material.getId())
                .setName(material.getName())
                .setDescription(material.getDescription())
                .setHandWritten(material.getHandWritten())
                .setPayPerMaterialEnabled(material.getIsPayPerMaterialEnabled())
                .setPages(material.getPages())
                .setPrice(material.getPrice())
                .setPrime(material.getIsPrime())
                .setRating(material.getRating())
                .setDownloadable(material.getDownloadable())
                .setDownloadCount(material.getDownloadCount())
                .setPrintable(material.getPrintable())
                .setPublic(material.getIsPublic())
                .setViewCount(material.getViewCount())
                .setVerified(material.getVerified())
                .setUploadedDate(material.getUploadedDate())
                .setAcademicSession(material.getAcademicSession());

        if(Objects.nonNull(material.getType())){
            materialModel.setType(material.getType().getLable());
        }

        if (Objects.nonNull(material.getContributor())){
            var contributor = UserModel.builder()
                    .id(material.getContributor().getId())
                    .fullName(material.getContributor().getFullName())
                    .build();
            materialModel.setContributedBy(contributor);
        }

        if (Objects.nonNull(material.getInstitute())){
            materialModel.setInstitute(material.getInstitute());
        }

        if (Objects.nonNull(material.getSubject())){
            materialModel.setSubject(material.getSubject());
        }

        if (RequestContext.isSecureRequest()) {
            var user = userService.getById(RequestContext.getUserId());
            if (userFavouritesRepository.existsByUserAndMaterial(user,material)){
                materialModel.setFavourite(true);
            }
            if (Objects.equals(material.getContributor().getId(), RequestContext.getUserId())) {
                materialModel.setOwner(true);
            } else {
                materialModel.setOwner(false);
            }
        }

        String coverUrl = MvcUriComponentsBuilder.fromController(PublicMaterialController.class)
                .path("/material-cover?id="+material.getId()).build().toString();
        String thumbUrl = MvcUriComponentsBuilder.fromController(PublicMaterialController.class)
                .path("/material-thumb?id="+material.getId()).build().toString();
        String url = MvcUriComponentsBuilder.fromController(PublicMaterialController.class)
                .path("/material-url?id="+material.getId()).build().toString();

        materialModel.setCoverUrl(coverUrl)
                .setThumbUrl(thumbUrl)
                .setUrl(url);
        return materialModel;
    }

    private void checkDocumentValidation(MultipartFile file) {
        Assert.isTrue(!file.isEmpty(), APIErrors.INVALID_REQUEST_PARAM.getMessage());
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        try {
            if (fileName.contains("..")) {
                throw new BadRequestException("Sorry! File name contains invalid path sequence " + fileName);
            }
        } catch (BadRequestException e) {
            e.printStackTrace();
        }
        if (!(fileFormatValid(file.getContentType()))) {
            throw new BadRequestException("Invalid file format!!");
        }

    }


    private static boolean fileFormatValid(String fileType) {
          final List<String> fileFormats = List.of(
                MediaType.APPLICATION_PDF_VALUE,
                MediaType.IMAGE_JPEG_VALUE,
                MediaType.IMAGE_PNG_VALUE,
                "application/msword");
        return fileFormats.contains(fileType);
    }


}
