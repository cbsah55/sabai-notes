package com.cbs.sabainotes.service.courses.impl;

import com.cbs.sabainotes.dto.courses.CourseDto;
import com.cbs.sabainotes.entity.courses.Course;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.model.courses.CourseModel;
import com.cbs.sabainotes.repository.courses.CourseRepository;
import com.cbs.sabainotes.service.courses.CourseService;
import com.cbs.sabainotes.utils.LocalStringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final FieldOfStudyServiceImpl fieldOfStudyService;

    @Override
    public CourseModel create(CourseDto courseDto){
        Assert.isTrue(!courseRepository.existsByName(courseDto.getName()),"course with same name already exists");
        var fieldOfStudy = fieldOfStudyService.getById(courseDto.getFieldOfStudyId());
        var course = new Course();
                course.setName(courseDto.getName());
                course.setSlug(LocalStringUtils.createSlug(courseDto.getName()));
                course.setFaculty(fieldOfStudy);
        return entityToModel(courseRepository.save(course));

    }

    @Override
    public CourseModel update(CourseDto courseDto){
        Assert.isTrue(!courseRepository.existsByName(courseDto.getName()),"course with same name already exists");
        var fieldOfStudy = fieldOfStudyService.getById(courseDto.getFieldOfStudyId());
        var course = getById(courseDto.getId());
        course.setName(courseDto.getName());
        course.setSlug(LocalStringUtils.createSlug(courseDto.getName()));
        course.setFaculty(fieldOfStudy);
        return entityToModel(courseRepository.save(course));
    }

    @Override
    public void deleteById(long id){
        var course = getById(id);
        courseRepository.delete(course);
    }
    @Override
    public CourseModel getModelById(long id){
        return entityToModel(getById(id));
    }

    @Override
    public List<CourseModel> getModels() {
        return courseRepository.findAll().stream().map(this::entityToModel).collect(Collectors.toList());
    }

    public Course getById(long id){
        return courseRepository.findById(id)
                .orElseThrow(()-> new NotFoundException(String.format("Course with id %d not found.",id)));
    }

    private CourseModel entityToModel(Course course){
        return CourseModel.builder()
                .id(course.getId())
                .name(course.getName())
                .slug(course.getSlug())
                .fieldOfStudy(course.getFaculty())
                .build();

    }
}
