package com.cbs.sabainotes.service.courses;

import com.cbs.sabainotes.entity.courses.FieldOfStudies;

import java.util.List;

public interface FieldOfStudyService {

    FieldOfStudies create(FieldOfStudies fieldOfStudies);

    void deleteById(long id);

    FieldOfStudies getById(long id);

    List<FieldOfStudies> getAll();

}
