package com.cbs.sabainotes.service.auth;

import com.cbs.sabainotes.dto.auth.AuthResponse;
import com.cbs.sabainotes.dto.auth.LoginRequestDto;
import com.cbs.sabainotes.dto.auth.SignUpDto;

public interface AuthService {
    AuthResponse authenticate(LoginRequestDto loginRequestDto);

    void register(SignUpDto signUpDto);

    void enableUser(long id);

    void passwordResetToken(String email);

    void resetPassword(String token, String newPassword);
}
