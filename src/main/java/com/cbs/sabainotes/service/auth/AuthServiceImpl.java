package com.cbs.sabainotes.service.auth;

import com.cbs.sabainotes.config.jwt.JwtService;
import com.cbs.sabainotes.dto.auth.AuthResponse;
import com.cbs.sabainotes.dto.auth.LoginRequestDto;
import com.cbs.sabainotes.dto.auth.SignUpDto;
import com.cbs.sabainotes.entity.user.PasswordResetToken;
import com.cbs.sabainotes.entity.user.Role;
import com.cbs.sabainotes.entity.user.User;
import com.cbs.sabainotes.entity.user.UserRole;
import com.cbs.sabainotes.exception.BadRequestException;
import com.cbs.sabainotes.exception.NotFoundException;
import com.cbs.sabainotes.exception.UserAlreadyExistsException;
import com.cbs.sabainotes.repository.RoleRepository;
import com.cbs.sabainotes.repository.user.PasswordResetTokenRepository;
import com.cbs.sabainotes.service.user.UserService;
import com.cbs.sabainotes.utils.EmailService;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.utility.RandomString;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.cbs.sabainotes.entity.user.UserRole.ROLE_USER;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService{

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;
    private final JwtService jwtService;
    private final PasswordResetTokenRepository passwordResetTokenRepository;
    private final EmailService emailService;



    @Override
    public AuthResponse authenticate(LoginRequestDto loginRequestDto) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDto.getEmail(),loginRequestDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtService.generateJwtToken(authentication);

        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        User user = userService.getByEmail(userDetails.getUsername());
        AuthResponse authResponse = new AuthResponse()
                .setName(user.getFullName())
                .setId(user.getId())
                .setEmail(user.getEmail())
                .setRoles(roles)
                .setJwt(jwt);
        return authResponse;
    }

    @Override
    public void register(SignUpDto signUpDto) {
        if (userService.existsByEmail(signUpDto.getEmail())){
            throw new UserAlreadyExistsException(String.format("user with email %s already exists.Please login.",signUpDto.getEmail()));
        }

        User user = new User();
                user.setEmail(signUpDto.getEmail());
                user.setFirstName(signUpDto.getFirstName());
                user.setLastName(signUpDto.getLastName());
                user.setPhone(signUpDto.getPhoneNo());
                user.setPassword(encoder.encode(signUpDto.getPassword()));
                user.setEnabled(false);
                if (isFirstUser()) {
                    user.setEnabled(true);
                    Role role = new Role();
                    role.setName(UserRole.ROLE_ADMIN);
                    role.setUser(user);
                    roleRepository.save(role);
                }else {
                    Role role = new Role();
                    role.setName(ROLE_USER);
                    role.setUser(user);
                    roleRepository.save(role);
                }
        userService.save(user);

    }

    @Override
    public void enableUser(long id){
        var user = userService.getById(id);
        user.setEnabled(true);
        userService.save(user);
    }

    @Override
    public void passwordResetToken(String email) {
        var user = userService.getByEmail(email);
        String token = RandomString.make(10);
        PasswordResetToken passwordResetToken = new PasswordResetToken();
        passwordResetToken.setUser(user);
        passwordResetToken.setExpiryDate(LocalDateTime.now().plusMinutes(10));
        passwordResetToken.setToken(token);
        passwordResetTokenRepository.save(passwordResetToken);

        try {
            emailService.sendPasswordResetToken(user.getEmail(),token);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void resetPassword(String token, String newPassword) {
        var passwordResetToken = passwordResetTokenRepository.findByToken(token)
                .orElseThrow(()-> new NotFoundException("Token invalid/expired.Request new token.."));
        var user = passwordResetToken.getUser();
        if (passwordResetToken.isExpired()){
            passwordResetTokenRepository.delete(passwordResetToken);
            throw new BadRequestException("Token is already expired. Please request a new token.");
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(newPassword));
        userService.save(user);
        passwordResetTokenRepository.delete(passwordResetToken);

    }

    private boolean isFirstUser(){
        return userService.isFirstUser();
    }

}
